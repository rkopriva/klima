# KLIMA #

Arduino Klima projekt.
Zaznamenání teploty, vlhkosti a plynu ze senzorů pomocí Arduina, odeslání na server přes WiFi v pravidelných intervalech a zobrazení grafů v prohlížeči.
Zvukové upozornění v případě překročení limitu. Možnost nastavení limitů vzdáleně přes serverovou část.

![alt text](https://bitbucket.org/rkopriva/klima/raw/4a0798bcc103c1cf4fbf4637977a589cd07ad79f/graf.jpg "Graf")

Nastavení:

![alt text](https://bitbucket.org/rkopriva/klima/raw/4a0798bcc103c1cf4fbf4637977a589cd07ad79f/nastaveni.jpg "Nastavení")

### Instalace Serveru ###

Nainstalujte a nakonfigurujte mysql server.
Nainstalujte nginx web server.
Nainstalujte ruby 2.7.0 nebo novější.
V souboru server/config/database.yml nastavte jméno a heslo pro přístup k databázi.
Ve složce server spusťte příkaz bundle install
Ve složce server spusťte příkaz rake db:migrate RAILS_ENV="production"
V souboru /etc/nginx/sites-enabled/default nastavte cestu k projektu, např.

server {
        listen 80 default_server;

        root /home/klima/server/public;

        index index.html index.htm index.nginx-debian.html;

        server_name _;

        passenger_enabled on;
        passenger_ruby /usr/bin/ruby2.7;
}
Restartujte nginx service nginx restart

### Instalace Klienta ###

Použitý Arduino modul je WeMos D1 R2 UNO ESP8266
Použité senzory: DHT 11 pro teplotu a vlhkost a MQ2 pro hořlavé plyny a kouř.
Pro zvukové upozornění je použit 5V bzučák.

Připojte D1 k počítači přes USB.
V Arduino studio otevřete projekt a nastavte IP adresu, jméno WiFi sítě a heslo.
Nahrejte projekt do D1.

![alt text](https://bitbucket.org/rkopriva/klima/raw/f9fd85fafc916a8e65dfb58b8b9b55ae3bd85313/klient/schema.jpg "Schema")