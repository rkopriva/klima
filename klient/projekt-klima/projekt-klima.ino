// D1 Klima

// includes
#include "ESP.h"
#include "ESP8266WiFi.h"
#include <WiFiClient.h>
#include <ArduinoHttpClient.h>
#include "DHT.h"
#include <ArduinoJson.h>

// definice pinu
#define pinLED 14
#define pinDHTData D9
#define pinDHTPwr D8
#define pinMQ2Data A0
#define pinPiezo D11
#define typeDHT DHT11
const char* host = "192.168.0.100";
const char* restURLSubmit = "/measures";
const char* restURLSettings = "/settings/get";

// d2 musi byt spojeny s reset pinem, aby fungovalo probuzeni

int intervalseconds = 30;

DHT myDHT(pinDHTData, typeDHT);

const char* nameWifi = "****";
const char* passwordWifi = "*****";

void setup() {
  Serial.begin(9600);
  Serial.println("Probuzeni");
  
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(pinLED, OUTPUT);
  pinMode(pinPiezo, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  digitalWrite(pinLED, LOW);
  digitalWrite(pinPiezo, LOW);

  // Nastaveni DHT senzoru
  pinMode(pinDHTPwr, OUTPUT);
  digitalWrite(pinDHTPwr, HIGH);

  // Nastaveni MQ2 senzoru
  pinMode(pinMQ2Data, INPUT);

  int wifiCounter = 0;
  Serial.println("Pripojuji k WiFi");
  WiFi.begin(nameWifi, passwordWifi);
  while (WiFi.status() != WL_CONNECTED && wifiCounter < 50) {
    delay(500);
    Serial.print(".");
  }
  if (WiFi.status() == WL_CONNECTED) {
    Serial.println("Connected to WiFi");

    // Nacteni nastaveni ze serveru
    WiFiClient wifi;
    HttpClient httpSettings = HttpClient(wifi, host, 80);
    httpSettings.beginRequest();
    httpSettings.get(restURLSettings);
    httpSettings.sendHeader("Content-Type", "application/json");
    httpSettings.sendHeader("Accept", "application/json");  
    httpSettings.endRequest();
    String payloadSettings = httpSettings.responseBody();
    httpSettings.stop();
    
    const size_t capacity = JSON_OBJECT_SIZE(8) + 160;
    DynamicJsonDocument settings(capacity);
    deserializeJson(settings, payloadSettings);
    intervalseconds = settings["intervalseconds"];
    int alerttype = settings["alerttype"];
    int temperaturealert = settings["temperaturealert"];
    int humidityalert = settings["humidityalert"];
    int gasalert = settings["gasalert"];
    
    myDHT.begin();

    // Precteni hodnot senzoru
    float temperature = myDHT.readTemperature();
    float humidity = myDHT.readHumidity();
    int gasRaw = analogRead(pinMQ2Data);
    
    if (isnan(temperature) || isnan(humidity) || isnan(gasRaw)) {
      Serial.println("Chyba pri cteni DHT senzoru!");
    } else {
      int gas = map(gasRaw, 0, 1024, 0, 100);

      
      // Odeslani hodnot na server
      String submitData = String("{\"temperature\": ") + temperature + ", \"humidity\": " + humidity + ", \"gas\": " + gas + "}";
      HttpClient httpSubmit = HttpClient(wifi, host, 80);
      httpSubmit.beginRequest();
      httpSubmit.post(restURLSubmit);
      httpSubmit.sendHeader("Content-Type", "application/json");
      httpSubmit.sendHeader("Accept", "application/json");  
      httpSubmit.sendHeader("Content-Length", submitData.length());
      httpSubmit.beginBody();
      httpSubmit.print(submitData);
      httpSubmit.endRequest();
      String response = httpSubmit.responseBody();
      Serial.println(submitData);
      Serial.println(response);
      httpSubmit.stop();

      if (alerttype == 1) {
         int flag = 0;
         if (gas > gasalert) {
             flag = flag + 4;
         }
         if (temperature > temperaturealert) {
             flag = flag + 3;
         }
         if (humidity > humidityalert) {
             flag = flag + 2;
         }
         if (flag > 0) {
            piezoAlert(flag);
         }
      }
    }
    digitalWrite(pinDHTPwr, LOW);
    digitalWrite(pinPiezo, LOW);
  }
  
  Serial.println("Usinam");
  ESP.deepSleep(intervalseconds * 1000 * 1000);
}

void piezoAlert(int count) {
    for (int i = 0; i < count; i++) {
        digitalWrite(pinPiezo, HIGH);
        delay(500);
        digitalWrite(pinPiezo, LOW);
        delay(500);
    }
}

void loop() {
}
