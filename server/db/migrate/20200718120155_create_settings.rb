class CreateSettings < ActiveRecord::Migration[6.0]
  def change
    create_table :settings do |t|
      t.integer :intervalseconds
      t.integer :alerttype
      t.float :temperaturealert
      t.float :humidityalert

      t.timestamps
    end
  end
end
