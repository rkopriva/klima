class MeasuresController < ApplicationController
  layout "measures"
  # GET /measures
  # GET /measures.json
  def index
    @measures = Measure.where('created_at >= ?', 2.day.ago.to_datetime).order("id")
  end

  def loadnew
    measures = Measure.where("id > ?", params[:id]).order("id")
    results = []
    
    measures.each do |measure|
      result = Hash.new
      result["id"] = measure.id
      result["temperature"] = measure.temperature
      result["humidity"] = measure.humidity
      result["gas"] = measure.gas
      result["timestamp"] = measure.created_at.to_time.to_i
      results << result
    end
    render :json => results
  end

  # POST /measures
  # POST /measures.json
  def create
    @measure = Measure.new(measure_params)

    respond_to do |format|
      if @measure.save
        render plain: 'OK'
      else
        render plain: 'ERROR'
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_measure
      @measure = Measure.find(params[:id])
    end

     # Only allow a list of trusted parameters through.
    def measure_params
      params.require(:measure).permit(:temperature, :humidity, :gas)
    end
end
