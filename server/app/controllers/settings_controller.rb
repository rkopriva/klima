class SettingsController < ApplicationController
  before_action :set_setting, only: [:edit, :update]

  # GET /settings
  # GET /settings.json
  def index
    @setting = Setting.find(1)
    render "edit"
  end

  def show
    @setting = Setting.find(1)
    render json: @setting
  end

  # GET /settings/1/edit
  def edit
  end

  # PATCH/PUT /settings/1
  # PATCH/PUT /settings/1.json
  def update
    respond_to do |format|
      if @setting.update(setting_params)
        format.html { redirect_to "/measures" }
        format.json { render json: @reservation.to_json }
      else
        format.html { render :edit }
        format.json { render json: @setting.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_setting
      @setting = Setting.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def setting_params
      params.require(:setting).permit(:intervalseconds, :alerttype, :temperaturealert, :humidityalert, :gasalert)
    end
end
