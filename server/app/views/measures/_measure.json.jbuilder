json.extract! measure, :id, :temperature, :humidity, :gas, :created_at, :updated_at
json.url measure_url(measure, format: :json)
