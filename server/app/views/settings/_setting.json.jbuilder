json.extract! setting, :id, :intervalseconds, :alerttype, :temperaturealert, :humidityalert, :gasalert, :created_at, :updated_at
json.url setting_url(setting, format: :json)
