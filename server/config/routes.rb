Rails.application.routes.draw do
  resources :settings
  resources :measures

  get 'measures/loadnew/:id', :to => 'measures#loadnew', :as => 'loadnew'

end
